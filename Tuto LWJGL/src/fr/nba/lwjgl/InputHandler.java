package fr.nba.lwjgl;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class InputHandler
{
	public static final int KEYBOARD_SIZE = Keyboard.getKeyCount();
	public static final int MOUSE_SIZE = Mouse.getButtonCount();
	
	private static boolean[] lastKeys = new boolean[KEYBOARD_SIZE];
	private static boolean[] lastButtons = new boolean[MOUSE_SIZE];
	
	public static void update()
	{
		for(int i = 0; i < KEYBOARD_SIZE; i++)
			lastKeys[i] = isKeyDown(i);
		
		for(int i = 0; i < MOUSE_SIZE; i++)
			lastButtons[i] = isButtonDown(i);
			
	}
	
	public static boolean isKeyDown(int key)
	{
		return Keyboard.isKeyDown(key);
	}
	
	public static boolean getKeyDown(int key)
	{
		return isKeyDown(key) && !lastKeys[key];
	}
	
	public static boolean getKeyUp(int key)
	{
		return !isKeyDown(key) && lastKeys[key];
	}
	
	public static boolean isButtonDown(int button)
	{
		return Mouse.isButtonDown(button);
	}
	
	public static boolean getButtonDown(int button)
	{
		return isButtonDown(button) && !lastButtons[button];
	}
	
	public static boolean getButtonUp(int button)
	{
		return !isButtonDown(button) && lastButtons[button];
	}
	
}
