package fr.nba.lwjgl;

public class Main
{
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	public static final String TITLE = "Tuto LWJGL";
	
	public static final double FPS_MAX = 60;
	
	public boolean run;
	private Game game;
	
	public Main()
	{
		Window.create(WIDTH, HEIGHT, TITLE);
	}
	
	public void start()
	{
		run = true;
		RenderUtil.init();
		game = new Game();
		loop();
	}
	
	public void loop()
	{
		boolean render = false;
		final double frameTime = 1.0 / FPS_MAX;
		
		long frames = 0;
		double frameCount = 0;
		
		long previousTime = Timer.getSystemTime();
		double unprocessedTime = 0;
		
		while(run)
		{
			
			long startTime = Timer.getSystemTime();
			long passedTime = startTime - previousTime;
			previousTime = startTime;
			
			unprocessedTime += passedTime /(double) Timer.SECOND;
			frameCount += passedTime;
			
			while(unprocessedTime > frameTime)
			{
				render = true;
				unprocessedTime -= frameTime;
				
				if(Window.isCloseRequested())
				{
					stop();
				}
				
				game.input();
				InputHandler.update();
				game.update();
				
				if(frameCount >= Timer.SECOND)
				{
					System.out.println(frames);
					frames = 0;
					frameCount = 0;
				}
				
			}
			
			if(render)
			{
				render();
				frames++;
				render = false;
			}
			else
			{
				try
				{
					Thread.sleep(1);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			
		}
		
		clean();
	}
	
	public void render()
	{
		RenderUtil.clear();
		game.render();
		Window.render();
	}
	
	public void stop()
	{
		run = false;
	}
	
	public void clean()
	{
		Window.destroy();
	}
	
	public static void main(String[] args)
	{
		Main window = new Main();
		window.start();
	}

}
