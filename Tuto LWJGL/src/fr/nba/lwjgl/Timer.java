package fr.nba.lwjgl;

public class Timer
{
	public static final long SECOND = 1000000000L;
	private static double delta;
	
	public static long getSystemTime()
	{
		return System.nanoTime();
	}
	
	public static double getDelta()
	{
		return delta;
	}
	
	public static void setDelta(double delta)
	{
		Timer.delta = delta;
	}

}
