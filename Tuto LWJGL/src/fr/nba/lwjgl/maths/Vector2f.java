package fr.nba.lwjgl.maths;

public class Vector2f
{
	private float x, y;
	
	public Vector2f(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public float length()
	{
		return (float)Math.sqrt(x * x + y * y);
	}
	
	public Vector2f normalize()
	{
		float length = length();
		
		float x_ = x / length;
		float y_ = y / length;
		
		return new Vector2f(x_, y_);
	}
	
	public Vector2f rotate(int angle)
	{
		double radianAngle = Math.toRadians(angle);
		float cosAngle = (float)Math.cos(radianAngle);
		float sinAngle = (float)Math.sin(radianAngle);
		
		return new Vector2f((x * cosAngle) - (y * sinAngle), (x * sinAngle) + (y * cosAngle));
	}
	
	public Vector2f add(float x, float y)
	{
		return new Vector2f(this.x + x, this.y + y);
	}
	
	public Vector2f add(Vector2f vector)
	{
		return new Vector2f(this.x + vector.getX(), this.y + vector.getY());
	}
	
	public Vector2f sub(float x, float y)
	{
		return new Vector2f(this.x - x, this.y - y);
	}
	
	public Vector2f sub(Vector2f vector)
	{
		return new Vector2f(this.x - vector.getX(), this.y - vector.getY());
	}
	
	public Vector2f multiply(float x, float y)
	{
		return new Vector2f(this.x * x, this.y * y);
	}
	
	public Vector2f multiply(Vector2f vector)
	{
		return new Vector2f(this.x * vector.getX(), this.y * vector.getY());
	}
	
	public Vector2f divide(float x, float y)
	{
		return new Vector2f(this.x / x, this.y / y);
	}
	
	public Vector2f divide(Vector2f vector)
	{
		return new Vector2f(this.x / vector.getX(), this.y / vector.getY());
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}
}
