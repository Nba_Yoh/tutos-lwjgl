package fr.nba.lwjgl.maths;

public class Vector3f 
{
	private float x, y, z;
	
	public Vector3f (float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public float length()
	{
		return (float)Math.sqrt(x * x + y * y + z * z);
	}
	
	public float dot(Vector3f vector)
	{
		return (this.x * vector.getX()) + (this.y * vector.getY()) + (this.z * vector.getZ());
	}
	
	public Vector3f cross(Vector3f vector)
	{
		float _x = (this.y * vector.getZ()) - (this.z * vector.getY());
		float _y = (this.z * vector.getX()) - (this.x * vector.getZ());
		float _z = (this.x * vector.getY()) - (this.y * vector.getX());
		
		return new Vector3f(_x, _y, _z);
	}
	
	public Vector3f normalize()
	{
		float length = length();
		
		float x_ = x / length;
		float y_ = y / length;
		float z_ = z / length;
		
		return new Vector3f (x_, y_, z_);
	}
	
	public Vector3f rotate(int angle)
	{
		return null;
	}
	
	public Vector3f add(float x, float y, float z)
	{
		return new Vector3f(this.x + x, this.y + y, this.z + z);
	}
	
	public Vector3f add(Vector3f vector)
	{
		return new Vector3f(this.x + vector.getX(), this.y + vector.getY(), this.z + vector.getZ());
	}
	
	public Vector3f sub(float x, float y, float z)
	{
		return new Vector3f(this.x - x, this.y - y, this.z - z);
	}
	
	public Vector3f sub(Vector3f vector)
	{
		return new Vector3f(this.x - vector.getX(), this.y - vector.getY(), this.z - vector.getZ());
	}
	
	public Vector3f multiply(float x, float y, float z)
	{
		return new Vector3f(this.x * x, this.y * y, this.z * z);
	}
	
	public Vector3f multiply(Vector3f vector)
	{
		return new Vector3f(this.x * vector.getX(), this.y * vector.getY(), this.z * vector.getZ());
	}
	
	public Vector3f divide(float x, float y, float z)
	{
		return new Vector3f(this.x / x, this.y / y, this.z / z);
	}
	
	public Vector3f divide(Vector3f vector)
	{
		return new Vector3f(this.x / vector.getX(), this.y / vector.getY(), this.z / vector.getZ());
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}
	
	public float getZ()
	{
		return z;
	}

	public void setZ(float z)
	{
		this.z = z;
	}
}
