package fr.nba.lwjgl.maths;

public class Matrix4f
{
	private float[][] matrix;
	
	public Matrix4f()
	{
		matrix = new float[4][4];
	}
	
	public Matrix4f createIdentity()
	{
		matrix[0][0] = 1;	matrix[0][1] = 0;	matrix[0][2] = 0;	matrix[0][3] = 0;
		matrix[1][0] = 0;	matrix[1][1] = 1;	matrix[1][2] = 0;	matrix[1][3] = 0;
		matrix[2][0] = 0;	matrix[2][1] = 0;	matrix[2][2] = 1;	matrix[2][3] = 0;
		matrix[3][0] = 0;	matrix[3][1] = 0;	matrix[3][2] = 0;	matrix[3][3] = 1;
		
		return this;
	}
	
	public Matrix4f multiply(Matrix4f mat)
	{
		Matrix4f result =  new Matrix4f();
		
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				result.set(i, j, (matrix[i][0] * mat.get(0, j)) +
							   	 (matrix[i][1] * mat.get(1, j)) +
							   	 (matrix[i][2] * mat.get(2, j)) +
							   	 (matrix[i][3] * mat.get(3, j)));
			}
		}
		
		return result;
	}
	
	public float get(int x, int y)
	{
		return matrix[x][y];
	}
	
	public void set(int x, int y, float value)
	{
		matrix[x][y] = value;
	}

	public float[][] getMatrix()
	{
		float[][] matrix = new float[4][4];
		
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				matrix[i][j] = this.matrix[i][j];
			}
		}
		
		return matrix;
	}

	public void setMatrix(float[][] matrix)
	{
		this.matrix = matrix;
	}
	
}
