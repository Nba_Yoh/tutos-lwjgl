package fr.nba.lwjgl;

import fr.nba.lwjgl.maths.Vector3f;

public class Game
{
	private MeshObject mesh;
	
	public Game()
	{
		mesh = new MeshObject();
		
		Vertex[] vertices = new Vertex[]{new Vertex(new Vector3f(-0.75f, -0.75f, 0)),
										  new Vertex(new Vector3f(0, 0.75f, 0)),
										  new Vertex(new Vector3f(0.75f, -0.75f, 0))};
		
		mesh.addVertices(vertices);
	}
	
	public void input()
	{
		
	}
	
	public void update()
	{
		
	}
	
	public void render()
	{
		mesh.draw();
	}
	
}
