package fr.nba.lwjgl;

import static org.lwjgl.opengl.GL11.*;

public class RenderUtil
{
	public static void clear()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	
	public static void init()
	{
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		glFrontFace(GL_CW);
		glCullFace(GL_BACK);
		glEnable(GL_CULL_FACE);
		
		glEnable(GL_DEPTH_TEST);
	}
}
